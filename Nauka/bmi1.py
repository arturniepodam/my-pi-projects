#!/usr/local/bin/python
# coding: UTF-8


print "To jest kalkulator wskaznika masy ciala BMI"
masa = (input("Podaj swoja wage w kg: "))
#masa = str(masa)
masa = float(masa)
wzrost = input("Podaj swoj wzrost w cm: ")
wzrostm = float(wzrost) / 100

wynik = float((masa / (wzrostm**2)))

print "                         --------------"
print "Twoj wskaznik BMI wynosi:", wynik
print "                         --------------"


if wynik >= 25:
    print "                          Uwaga! Masz nadwage"
elif wynik < 24.99 and wynik > 18.5:
    print "                          Waga w normie."
else:
    print "                          Niedowaga"    

zakres_dolny = float((wzrostm**2) * 18.5)
zakres_gorny = float((wzrostm**2) * 24.99)

print 15 * "..."
print "Przy wzroscie %d cm, przedzial prawidlowej wagi" % wzrost
print "miesci sie miedzy: %.1f a %.1f kg" % (zakres_dolny, zakres_gorny)

print 15 * "..."

print " "
legenda = raw_input("Jezeli chcesz poznac wytlumaczenie wcisnij 't'\
jezeli nie chcesz wcisnij 'n':")

if legenda == "t":
    print """
	LEGENDA:

	Dla osób doroslych wartosc BMI wskazuje na:
	    < 18,5 - niedowage
	    18,5 - 24,99 - wartosc prawidlowa
	    >= 25,0 - nadwage
	    
	Dziekujemy za skorzystanie z kalkulatora"""
else:
    print "Dziekujemy za skorzystanie z kalkulatora"
